package com.yue.ar.suite;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

public class ARSuite extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;

	private MyActor actor;

	private Stage stage;

	@Override
	public void create () {
		img = new Texture("badlogic.jpg");
		actor = new MyActor(new TextureRegion(img));

		stage = new Stage();
		stage.addActor(actor);
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//更新演員
		stage.act(Gdx.graphics.getDeltaTime());

		// 绘制舞台，并批处理舞台中的演员（自动逐个调用演员的 draw() 方法绘制演员）
		stage.draw();
	}
	
	@Override
	public void dispose () {
		// 释放纹理资源
		if (img != null) {
			img.dispose();
		}
		// 释放舞台资源
		if (stage != null) {
			stage.dispose();
		}

	}
}
